# Nginx configurations files and scripts
## Prepare configs

```bash
sudo ./cp-config.sh
```

## Test and restart nginx

```bash
sudo nginx -t
sudo systemctl restart nginx
```

## Add certbot

1. Install certbot for nginx

```bash
sudo apt install certbot python3-certbot-nginx
```

2. Run certbot

```bash
sudo certbot --nginx
```

